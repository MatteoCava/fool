grammar FOOL;

@header{
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import ast.*;
import ast.oo.*;
import lib.*;
}

/* variabili globali per il parser */
@parser::members{
	
private int nestingLevel = 0;
/* 	Lista di mappe stringa-id a stentry che contiene info sulla dichiarazione.
 Lista di mappe perche' ne abbiamo una per livello fino ad arrivare a quello globale */
private ArrayList<HashMap<String,STentry>> symTable = new ArrayList<HashMap<String,STentry>>();
/*  Livello ambiente con dichiarazioni piu' esterno e' 0 (prima posizione ArrayList) invece che 1 (slides)
 il "fronte" della lista di tabelle e' symTable.get(nestingLevel) */

// Mappa il nome di ogni classe nella propria virtual table, resta anche dopo dichiarazione della classe
private HashMap<String, HashMap<String,STentry>> classTable = new HashMap<String, HashMap<String,STentry>>();
 //va condiviso tra classi e dichairazioni di var/fun

}


/* variabili globali per il lexer */
@lexer::members {
	int lexicalErrors=0;
}

/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/

prog returns [Node ast]
	: {HashMap<String,STentry> hm = new HashMap<String,STentry> ();
       symTable.add(hm);}          
	  ( e=exp 	
        {$ast = new ProgNode($e.ast);} 
      | LET {ProgLetInNode progLetInNode = new ProgLetInNode();}
      	( c=cllist {progLetInNode.setCllist($c.astlist);} (d=declist {progLetInNode.setDeclist($d.astlist);})? 
      		| d=declist {progLetInNode.setDeclist($d.astlist);}) IN e=exp  {progLetInNode.setExp($e.ast);} 
        {
        	$ast = progLetInNode;	
        }      
	  ) 
	  {symTable.remove(nestingLevel);}
      SEMIC ;
      
cllist returns [ArrayList<Node> astlist]       
		: { $astlist = new ArrayList<Node>();
			int offset = -2; 
		}
		( CLASS classId=ID 
			{
				
				HashMap<String, STentry> virtualTable = new HashMap<>();
				
				
				ClassNode classNode = new ClassNode($classId.text); //da aggiungere par e methods, alla fine aggiungere il type e poi inserire in astlist
				STentry entry = new STentry(nestingLevel, new ClassTypeNode(), offset--);
				HashMap<String, STentry> hashMapForSymbolTable = symTable.get(nestingLevel);
				//aggiungere la coppia classId-stentry alla classtable, in seguito classtypenode dovra' essere arricchito e aggiunto alla entry
				if ( hashMapForSymbolTable.put($classId.text,entry) != null  ){
					System.out.println("Class id "+$classId.text+" at line "+$classId.line+" already declared");
              		System.exit(0);
              	}
              	
              	int fieldsOffset = -1;
              	int methodOffset = 0;
              	  
			}
			
			(EXTENDS superClassId=ID
				{
					
					//aggiunta gerarchia a mappa superType 
					FOOLlib.addSuperType($classId.text, $superClassId.text);
					
					// 0 perche le classi sono tutte a nesting level 0
					STentry superEntry = symTable.get(0).get($superClassId.text);
					if (superEntry == null) {
						System.out.println("SuperClass id " + $superClassId.text + " at line " + $superClassId.line + " not declared");
              			System.exit(0);
					}
					
					// ottenimento virtual table da class table, in corrispondeza della classe padre
					virtualTable = FOOLlib.cloneVirtualTable(classTable.get($superClassId.text));

					// eredito il tipo della classe padre
					ClassTypeNode superClassTypeNode = (ClassTypeNode) superEntry.getType();
					
					// aggiunta in virtualtable dei campi e metodi ereditati
					((ClassTypeNode) entry.getType()).addFieldsType(superClassTypeNode.getAllFields());
					((ClassTypeNode) entry.getType()).addMethodsType(superClassTypeNode.getAllMethods());
					hashMapForSymbolTable.put($classId.text,entry);
					
					//aggiorno gli offset in base ai campi e metodi ereditati
					fieldsOffset -= superClassTypeNode.getAllFields().size();
					methodOffset += superClassTypeNode.getAllMethods().size();  
					
					classNode.setSuperEntry(superEntry);	 
				}
			)? 
			{
				nestingLevel++;
				//aggiunta nuovo livello in symTable
				classTable.put($classId.text, virtualTable);
				symTable.add(virtualTable);
			}
			
			
			//parametri
			
			{HashSet<String> fieldsMethodsNames = new HashSet(); }
		
			LPAR 
				(parId=ID COLON parType=type 
					{	
						
						//ottimizzazione parsing: ridefinizione erronea di campi e metodi
						if (fieldsMethodsNames.contains($parId.text)) {
							System.out.println("Wrong overriding. Trying to override method or parameter already declared " + $parId.text + " at line " + $parId.line );
              				System.exit(0);
						} else {
							fieldsMethodsNames.add($parId.text);
						}
						
						
						int fieldOffsetToAdd;

						if (virtualTable.containsKey($parId.text)) {	//overriding
							STentry parentEntry = virtualTable.get($parId.text);
							
                 			fieldOffsetToAdd = parentEntry.getOffset(); // preservo l'offset della vecchia stentry
                 			
                 			if (parentEntry.isMethod()) {
                 				System.out.println("Wrong overriding. Trying to override method with parameter " + $parId.text + " at line " + $parId.line);
              					System.exit(0);
                 			}                 			
                 			
                 			//controllo tipo parametro, dovra essere stessotipo o sottotipo
                 			if (!FOOLlib.isSubtype($parType.ast, parentEntry.getType())) {
                 				System.out.println("Wrong parameter type for " + $parId.text + " at line " + $parId.line + ", expected type or a subtype of " + parentEntry.getType().toPrint(""));
              					System.exit(0);
                 			}
                 			
                 			
                 		} else {
                 			fieldOffsetToAdd = fieldsOffset--;
                 		}
						
						STentry fieldSTentry = new STentry(nestingLevel, $parType.ast, fieldOffsetToAdd);
						//aggiunta in virtual table, quindi anche in classTable
						virtualTable.put($parId.text, fieldSTentry);
						
						
						//aggiunta campo a classnode
						FieldNode fieldNode = new FieldNode($parId.text, $type.ast, fieldOffsetToAdd);
						
						int fieldListIndex = - fieldOffsetToAdd - 1;
						classNode.addField(fieldNode);
						
						((ClassTypeNode) entry.getType()).addOrOverrideFieldType(fieldListIndex, $parType.ast);
						
					} 
					
					(COMMA parId=ID COLON parType=type
						{	
							//ottimizzazione parsing: ridefinizione erronea di campi e metodi
							if (fieldsMethodsNames.contains($parId.text)) {
								System.out.println("Wrong overriding. Trying to override method or parameter already declared " + $parId.text + " at line " + $parId.line );
	              				System.exit(0);
							} else {
								fieldsMethodsNames.add($parId.text);
							}

							if (virtualTable.containsKey($parId.text)) { //overriding
	                 			STentry parentEntry = virtualTable.get($parId.text);
							
								//preservo offset della vecchia stentry
	                 			fieldOffsetToAdd = parentEntry.getOffset();
	                 			
	                 			if (parentEntry.isMethod()) {
                 					System.out.println("Wrong overriding. Trying to override method with parameter " + $parId.text + " at line " + $parId.line);
              						System.exit(0);
                 				}
                 				
	                 			//controllo tipo parametro, dovra essere stessotipo o sottotipo
	                 			if (!FOOLlib.isSubtype($parType.ast, parentEntry.getType())) {
	                 				System.out.println("Wrong parameter type for " + $parId.text + " at line " + $parId.line + ", expected type or a subtype of " + parentEntry.getType().toPrint(""));
	              					System.exit(0);
	                 			}
	                 		} else {
	                 			fieldOffsetToAdd = fieldsOffset--;
	                 		}
							
							fieldSTentry = new STentry(nestingLevel, $parType.ast, fieldOffsetToAdd);
							//aggiungo alla classTable, controllo che non ci sia paramentro uguale
							virtualTable.put($parId.text, fieldSTentry);
                  			
						
							//aggiunta campo a classnode
							fieldNode = new FieldNode($parId.text, $type.ast, fieldOffsetToAdd);
							
							int index = - fieldOffsetToAdd -1;
							classNode.addField(fieldNode);
							
							((ClassTypeNode) entry.getType()).addOrOverrideFieldType(index, $parType.ast);
							
						}
						
					)*
				)? 
			
			RPAR    
			
				//SEZIONE METODI
              CLPAR
                 ( FUN methodId=ID COLON methodType=type 
                 	{
                 		
                 		//ottimizzazione parsing: ridefinizione erronea di campi e metodi
                 		if (fieldsMethodsNames.contains($methodId.text)) {
							System.out.println("Wrong overriding. Trying to override method or parameter already declared " + $methodId.text + " at line " + $methodId.line );
              				System.exit(0);
						} else {
							fieldsMethodsNames.add($methodId.text);
						}
                 		
                 		
                 		ArrayList<Node> parList = new ArrayList<>();
                 		
                 		int methodOffsetToAdd;
                 		
                 		//controllo se metodo va in override di un metodo della classe padre
                 		if (virtualTable.containsKey($methodId.text)) {
                 			STentry parentMethodEntry = virtualTable.get($methodId.text);
                 			
                 			if (!parentMethodEntry.isMethod()) {
                 				System.out.println("Wrong overriding. Trying to override parameter with method " + $methodId.text + " at line " + $methodId.line);
              					System.exit(0);
                 			}
                 			
                 			methodOffsetToAdd = parentMethodEntry.getOffset();
                 			
                 			
                 		} else {
                 			methodOffsetToAdd = methodOffset++;
                 		}
                 		
                 		ArrowTypeNode arrowNode = new ArrowTypeNode(parList, $methodType.ast); //metodo, i tipi dei campi verranno aggiunti successivamente 
                 		STentry methodEntry = new STentry(nestingLevel, arrowNode, methodOffsetToAdd);
                 		methodEntry.setAsMethod();
                 		
                 		//aggiungo alla virtualTable
						virtualTable.put($methodId.text, methodEntry);
						 
						
						MethodNode methodNode = new MethodNode($methodId.text, methodEntry.getOffset(), $methodType.ast);
						methodNode.setSymType(arrowNode);
                 	
                 	}
                 	{ 
                 		int parMethodOffset = 1;
                 		nestingLevel++;
                 		symTable.add(new HashMap<String,STentry> ());
                 		ArrayList<Node> methodParList = new ArrayList<Node>();
                 	}
                 	//paramentri funzione
                 	LPAR (methodParId=ID COLON methodParType=hotype 
                 		{
							ParNode mpar = new ParNode($methodParId.text,$methodParType.ast); 
			                STentry methodSTentry = new STentry(nestingLevel,$methodParType.ast,parMethodOffset++);                  
			                symTable.get(nestingLevel).put($methodParId.text, methodSTentry);
			                methodParList.add(mpar);
			                parList.add($methodParType.ast);
		                  
                 		}
                 		(COMMA methodParId=ID COLON methodParType=hotype
                 			{
                 				ParNode mpar2 = new ParNode($methodParId.text,$methodParType.ast);
				                methodSTentry = new STentry(nestingLevel,$methodParType.ast,parMethodOffset++);                                
				                if ( symTable.get(nestingLevel).put($methodParId.text, methodSTentry) != null  ) {
				                	System.out.println("Method Parameter id "+$methodParId.text+" at line "+$methodParId.line+" already declared");
				                   	System.exit(0);
				                }
				                methodParList.add(mpar2);
				                parList.add($methodParType.ast);
                 			}
                 			
                 		)*
                 		{methodNode.addParList(methodParList);}
                 	
                 	
                 	)? RPAR 
                 	{
                 		ArrayList<Node> methodDecList = new ArrayList<Node>();
                 		int methodDecOffset = -2;
                 		
                 	}
                 	//dichiarazioni all'interno della funzione
	                     (LET (VAR methodParId=ID COLON methodDecVarType=type ASS methodParExp=exp SEMIC
	                     	{
	                     		VarNode methodVarNode = new VarNode($methodParId.text, $methodDecVarType.ast, $methodParExp.ast);
	                     		methodDecList.add(methodVarNode);
	                     	                               
            					HashMap<String,STentry> hm = symTable.get(nestingLevel);
             					if ( hm.put($methodParId.text,new STentry(nestingLevel,$methodDecVarType.ast,methodDecOffset--)) != null  ) {
             						System.out.println("Method Var id "+$methodParId.text+" at line "+$methodParId.line+" already declared");
              						System.exit(0);
              					}
	                     		
	                     	}
	                     	
	                     	
	                     )+ {methodNode.addDecList(methodDecList);}
	                     //corpo funzione
	                     IN)? methodExp=exp 
	                     {
	                     	methodNode.addBody($methodExp.ast);
	                     }
        	       SEMIC
        	       {
        	       		symTable.remove(nestingLevel--);
        	       		
        	       		//OVERRIDE METODI 
        	       		int methodListIndex = methodOffsetToAdd;
        	       		
        	       		
        	       		classNode.addMethod(methodNode);
        	       		((ClassTypeNode)entry.getType()).addOrOverrideMethodType(methodListIndex, arrowNode); 
        	       		
        	       }
        	     )*   
        	      {classNode.setSymType(entry.getType());}       
              CRPAR
              
          { 
          	symTable.remove(nestingLevel--);
			$astlist.add(classNode);
          }
          )+
        ; 

/* una o piu' dichiarazioni: iniziano con var o fun -> in ogni caso finisce con SEMIC */
declist	returns [ArrayList<Node> astlist]   
	:
	{
		/* creazione lista vuota per poi poterli inserire piu' in basso */
		$astlist = new ArrayList<Node>();
		
		/* variabile intera che  la mettiamo nella pallina e decrementiamo ogni volta che incontriamo la dichiarazione di variabile
		   non usando le funzioni sarebbe partita da -1, poi viene messa a -2 per armonizzare con il layout per le funzioni */
		int offset=-2;
		
		if (nestingLevel == 0) {
			offset -= classTable.size();
		}
	}
	( 
		/* e' una sequenza di una o piu' dichiarazioni, ognuna di esse e' o comincia con var o con fun e alla fine in ogni caso finisce con semicol */
	  	(
	  		VAR i=ID COLON vart=hotype ASS e=exp 
            {
	  			/* ovviamente rappresenta la dichiarazione di variabili */
            	VarNode v = new VarNode($i.text,$vart.ast,$e.ast);
            	/* aggiungo alla declist */
             	$astlist.add(v);
             	
             
             	
             	HashMap<String,STentry> hm = symTable.get(nestingLevel);
             	
             	/* devo guardare se c'e' e nel caso sostituirla, altrimenti aggiungerla
             	   non controllo perche' il put di java se c'e' gia' ritorna il valore vecchio (quindi lo mette e mi dice se c'era gia') */
             	if ( hm.put($i.text,new STentry(nestingLevel,$vart.ast,offset--)) != null ){	/* creazione della pallina per le dichiarazioni di variabili */
             		
             		/* line e' il numero di linea del sorgente dove ha matchato, sono informazioni che il lexer raccoglie */
             		System.out.println("Var id "+$i.text+" at line "+$i.line+" already declared");
             		System.exit(0);
             	}    
             	
             	// se si tratta di ArrowType, considero offset doppio
               	if ($vart.ast instanceof ArrowTypeNode) {
            		offset--;
               	}         
            }  
	  		|  
	  		/* la parte della fun al completo e':  il punto interrogativo mi dice che quello tra le parentesi tonde della fun e' opzionale(possono non esserci parametri)
	  		 * anche let in nella funzione e' opzionale perche' potrebbe non avere dichiarazioni di var
	  		 * NON c'e' il return, il programma e' un'espressione che ritorna un valore e la funzione e' un qualcosa che ha un corpo con un'espressine che quando ritorna mi da il risultato della fun
	  		 * FUN ID COLON type LPAR (ID COLON type (COMMA ID COLON type)* )? RPAR (LET declist IN)? exp
	  		 * '?' mi dice che che tra le parentesi potrebbe non esserci nulla, dentro poi posso avere o un parametro solo o tanti
	  		 * (con la ',' che ci deve essere solo quando ho piu' di un parametro)
	  		 * nella parte let in opzionale riuso la variabile declicst
	  		 */
	  		FUN i=ID COLON t=type
	  		{
	  			/* inserimento di ID nella symtable
	  			   nel funnode non devo aggiungere subito tutti i parametri ma posso farlo man mano */		
	  			FunNode f = new FunNode($i.text,$t.ast);      
               	$astlist.add(f);
               	
               	/* STESSA COSA DELLA DICHIARAZIONE DI VARIABILE MA APPLICATA ALLE FUNZIONI */
               	HashMap<String,STentry> hm = symTable.get(nestingLevel);
               	             	               	
               	/* anche qui mi aggiungo l'informazione sull'offset*/
               	STentry entry = new STentry(nestingLevel, offset--);
               	if ( hm.put($i.text,entry) != null  ) {
               		System.out.println("Fun id " + $i.text + " at line " + $i.line + " already declared");
              		System.exit(0);
              	}
             	offset--;
               	
               	/* ENTRIAMO IN UN NUOVO SCOPE, prima della dichiarazione dei parametri:
               	   creare una nuova hashmap per la symTable da usare per il nuovo livello */
                nestingLevel++;
                HashMap<String,STentry> hmn = new HashMap<String,STentry>();	/* nhm sta per newhashmap */
                symTable.add(hmn);
			}
	  		
	  		/* da qui cominciano i parametri
	  		 * quando incontriamo una dichiarazione di un parametro e' come una dichairazione di una variabile ma senza exp. 
	  		 * bisogna quindi creare il ParNode (classe per la dichairazione di paramentri praticamente uguale al VarNode ma senza exp) 
	  		 * mettendolo come in FunNOde avremo una funzione addPAr e quini una var parlist
	  		 * 
	  		 *   campo "parlist" che e' una lista di Node (inizializzato ad una lista vuota)
	  		 *   metodo "addPar()" che aggiunge un nodo al campo parList
	  		 */
	  		LPAR 
	  		{
	  			 ArrayList<Node> parTypes = new ArrayList<Node>();
	  			 int paroffset=1;	/* variaible che uso per gli offset delle variabili */
	  		}
	  		/* qui abbiamo i parametri, offset che va da 1 a n */
	  		( fid=ID COLON fty=hotype
	  			{ 
	  				parTypes.add($fty.ast);
	                /* aumento offset di 2 nel caso sia di tipo funzionale */
	               	if ($fty.ast instanceof ArrowTypeNode) {
	               		paroffset++;
	               	}
	  				/* creero oggetto ParNode e lo aggiungo al fun node col metodo che sara' da fare */
	                ParNode fpar = new ParNode($fid.text,$fty.ast);		/* creo nodo ParNode */
	                f.addPar(fpar);										/* lo attacco al FunNode con addPar */
	                
	                if ( hmn.put($fid.text,new STentry(nestingLevel,$fty.ast,paroffset++)) != null  ) {		/* aggiungo dich a hmn */
	                	System.out.println("Parameter id " + $fid.text + " at line " + $fid.line + " already declared");
	                   	System.exit(0);
	               	}
	    
	  			}
		  		/* creare il ParNode
		  		 * lo attacco al FunNode invocando addPar
		  		 * aggiungo una STentry alla hashmap nhm
		  		 */
	  			( COMMA id=ID COLON ty=hotype
	  				{
	  					parTypes.add($ty.ast);
	                    ParNode par = new ParNode($id.text,$ty.ast);
	                    f.addPar(par);
	                 
	                    /* aumento offset di 2 nel caso sia di tipo funzionale */
		               	if ($ty.ast instanceof ArrowTypeNode) {
		               		paroffset++;
		               	}
	                    
	                    if ( hmn.put($id.text,new STentry(nestingLevel,$ty.ast,paroffset++)) != null  ) {
	                    	
	                    	/* non controllo perche' il put di java se c'e' gia' ritorna il valore vecchio (quindi lo mette e mi dice se c'era gia') */
	                    	System.out.println("Parameter id "+$id.text+" at line "+$id.line+" already declared");
	                     	System.exit(0);
	                    } 
                    }
				)*
            )? 
            /* FINO A QUI ESERCITAZIONE */
            RPAR 
            {
	            /* addType per rimpire il campo della stentry che era rimasto vuoto */
	            ArrowTypeNode symType = new ArrowTypeNode(parTypes,$t.ast); 
	  			entry.addType(symType);
	  			((FunNode)f).setSymType(symType);
	  			
	  		}
            (LET d=declist IN {f.addDec($d.astlist);})? e=exp // add dec se ci sono delle dichiarazioni, altrimenti non c'e' nulla
            	{
              	f.addBody($e.ast);
              	/* esco dallo scope
              	 * rimuovere la hashmap corrente poiche' esco dallo scope
              	 */
               	symTable.remove(nestingLevel--);	/* post decremento (come fare la remove e poi decrementare) */
            	}
	  		) 
	  	SEMIC
    )+          
;
 
type	returns [Node ast]
  : INT  {$ast=new IntTypeNode();}
  | BOOL {$ast=new BoolTypeNode();} 
  | classId=ID  {$ast = new RefTypeNode($classId.text);}
	;	


exp	returns [Node ast]
 	: f=term {$ast= $f.ast;}
 	    (PLUS l=term
 	     {$ast= new PlusNode($ast,$l.ast);}
 	     | MINUS l=term 
 	     {$ast= new MinusNode($ast,$l.ast);}
 	     | OR l=term  
 	     {$ast = new OrNode($ast,$l.ast); }
 	    )*
 	;
 	
term	returns [Node ast]
	: f=factor {$ast= $f.ast;}
	    (TIMES l=factor
	     {$ast= new MultNode ($ast,$l.ast);}
	     | DIV  l=factor 
	     {$ast= new DivNode ($ast,$l.ast);}
  	     | AND  l=factor
  	     {$ast = new AndNode($ast,$l.ast); } 
	    )*
	;
	
factor	returns [Node ast]
	: f=value {$ast= $f.ast;}
	    (EQ l=value 
	     {$ast= new EqualNode ($ast,$l.ast);}
		| GE l=value 
		{$ast= new GreaterEqualNode ($ast,$l.ast);}
		| LE l=value 
		{$ast= new LessEqualNode ($ast,$l.ast);}
	    )*
 	;	 	
 
value returns [Node ast]:
	n=INTEGER   
		{
			$ast = new IntNode(Integer.parseInt($n.text));
		}  
	| TRUE
		{
			$ast = new BoolNode(true);
		}  
	| FALSE
	  {$ast= new BoolNode(false);}  
	| LPAR e=exp RPAR    
	  {$ast= $e.ast;}  
	| NULL	    
		{$ast = new EmptyNode();}
	| NEW classId=ID 
			{
				STentry classEntry = symTable.get(0).get($classId.text);
				if (classEntry == null) {
					System.out.println("Class "+$classId.text+" at line "+$classId.line+" not declared");
			        System.exit(0);
				}
				
				ArrayList<Node> argList = new ArrayList<Node>();
			}
			LPAR (e=exp
					{argList.add($e.ast);}
					(COMMA e=exp
							{argList.add($e.ast);}
					)* 
				 )? {$ast = new NewNode($classId.text,classEntry,argList);} RPAR  
		
	| IF x=exp THEN CLPAR y=exp CRPAR 
		   ELSE CLPAR z=exp CRPAR 
	  {$ast= new IfNode($x.ast,$y.ast,$z.ast);}	 
	| NOT LPAR e=exp RPAR
		{$ast = new NotNode($e.ast);}
	| PRINT LPAR e=exp RPAR	
	  {$ast= new PrintNode($e.ast);}
	| LPAR exp RPAR
	| i=ID 
	  {
			/* cerco la dichiarazione nella tabella dei simboli
			 * ciclo su tutti gli scope, decrementando il nesting level, finche non trovo la dichiarazione.
			 * se non la trovo manco al nesting level 0, do un errore.
			 */
           int j=nestingLevel;
           STentry entry=null;		/* quando trovo la pallina facendo la ricerca la metto qua */
           while (j>=0 && entry==null) {
        	   
        	   /* se c'e' , entry prende valore e esco, se no decremento e se non trovo esco dal while. */
        	   entry=(symTable.get(j--)).get($i.text);  /* cerco l'id nella lista di tabella andando gia' di nesting level */
           }
           
           /* aggiungiamo nel node l'informazione relativa al nesting level in cui siamo
              se poi saltera' fuori che e' una funzione verra' sovrascritto quindi no problem */
          
          if (classTable.keySet().contains($i.text)) {
          	System.out.println("Wrong id " + $i.text + " for IdNode, cannot be a class name");
			System.exit(0);
          } 
           $ast = new IdNode($i.text,entry,nestingLevel);
        }
	  /* dopo ID opzionalmente posso avere una coppia di parentesi tonde (che mi dice che e' una chiamata di funzione)
	   	 * con dentro:
	   	 * - o nulla (chiamata a funzione senza parametri) 
	   	 * - o 1 o piu' di uno separati e preceduti da virgola 
	   	 */
	   ( LPAR
			{
				ArrayList<Node> arglist = new ArrayList<Node>();	/* array per collezionare i nodi generati dalle espressioni */
			}  
	   	 	(a=exp
	   	 		{
	   	 			arglist.add($a.ast);
	   	 		} 
	   	 	(COMMA a=exp 
	   	 		{
	   	 			arglist.add($a.ast);
	   	 		}
	   	 	)* 	// per il primo niente COMMA, per tutti gli altri serve.
	   	) 
	   	? 
	   	RPAR 
	   	/* entry e' quello usato per fare l'id node */
	   	{
	   	 	/* aggiungiamo nel node l'informazione relativa al nesting level in cui siamo */
	   	 	$ast = new CallNode($i.text,entry,arglist,nestingLevel);
	   	} 
	   	 //classi
		 | DOT classMethodId=ID LPAR 
			{
				ArrayList<Node> arglist = new ArrayList<Node>();
				STentry methodEntry = null;
				if (entry != null && entry.getType() instanceof RefTypeNode) {
					String classId = ((RefTypeNode)entry.getType()).getClassId();
					methodEntry = classTable.get(classId).get($classMethodId.text);
					if (methodEntry == null) {
						System.out.println("Method " + $classMethodId.text + " of class "+ classId + " not found");
						System.exit(0);
					}
					
				} else {
					System.out.println("Id "+$i.text+" at line "+$i.line+" not an object");
            		System.exit(0);
				}
			}
			(e=exp 
				{
					arglist.add($e.ast);
				}
				(COMMA e=exp
		   	 		{arglist.add($e.ast);}
				)*
		   	)? {$ast = new ClassCallNode($i.text,entry,methodEntry,arglist,nestingLevel); } RPAR  
	   	 
		)?


 	; 
 	
hotype returns [Node ast] : t=type {$ast = $t.ast;}
       	| a=arrow {$ast = $a.ast;}
        ;
        
arrow returns [Node ast]: 
	{ArrayList<Node> parTypes = new ArrayList<Node>();}
	LPAR (
		firsttype=hotype 
		{		
			parTypes.add($firsttype.ast);
		}
		(COMMA 
			othertype=hotype
			{parTypes.add($othertype.ast);}
		)*
	)? RPAR ARROW returntype=type {$ast = new ArrowTypeNode(parTypes, $returntype.ast);}
	; 
  		
/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/

PLUS  	: '+' ;
MINUS   : '-' ;
TIMES   : '*' ;
DIV 	: '/' ;
LPAR	: '(' ;
RPAR	: ')' ;
CLPAR	: '{' ;
CRPAR	: '}' ;
SEMIC 	: ';' ;
COLON   : ':' ; 
COMMA	: ',' ;
DOT	    : '.' ;
OR	    : '||';
AND	    : '&&';
NOT	    : '!' ;
GE	    : '>=' ;
LE	    : '<=' ;
EQ	    : '==' ;	
ASS	    : '=' ;
TRUE	: 'true' ;
FALSE	: 'false' ;
IF	    : 'if' ;
THEN	: 'then';
ELSE	: 'else' ;
PRINT	: 'print' ;
LET     : 'let' ;	
IN      : 'in' ;	
VAR     : 'var' ;
FUN	    : 'fun' ; 
CLASS	: 'class' ; 
EXTENDS : 'extends' ;	
NEW 	: 'new' ;	
NULL    : 'null' ;	  
INT	    : 'int' ;
BOOL	: 'bool' ;
ARROW   : '->' ; 	
INTEGER : '0' | ('-')?(('1'..'9')('0'..'9')*) ; 

ID  	: ('a'..'z'|'A'..'Z')('a'..'z' | 'A'..'Z' | '0'..'9')* ;


WHITESP  : ( '\t' | ' ' | '\r' | '\n' )+    -> channel(HIDDEN) ;

COMMENT : '/*' (.)*? '*/' -> channel(HIDDEN) ;
 
ERR   	 : . { System.out.println("Invalid char: "+ getText()); lexicalErrors++; } -> channel(HIDDEN); 
