package ast;

public class BoolTypeNode implements Node {

	public BoolTypeNode() {
	}

	public String toPrint(String s) {
		return s + "BoolType\n";
	}

	// non utilizzato
	@Deprecated
	public Node typeCheck() {
		return null;
	}

	// non utilizzato
	@Deprecated
	public String codeGeneration() {
		return "";
	}

}