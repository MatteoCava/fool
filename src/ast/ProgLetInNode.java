package ast;

import java.util.ArrayList;

import lib.FOOLlib;

public class ProgLetInNode implements Node {

	private ArrayList<Node> cllist = new ArrayList();
	private ArrayList<Node> declist = new ArrayList();
	private Node exp;

	public ProgLetInNode() {
	}
	
	public void setExp(Node e) {
		this.exp = e;
	}
	
	public void setCllist(ArrayList<Node> cllist) {
		this.cllist = cllist;
	}

	public void setDeclist(ArrayList<Node> declist) {
		this.declist = declist;
	}

	public String toPrint(String s) {
		String clstr = "";
		for (Node cl : cllist) {
			clstr += cl.toPrint(s + "  ");
		}
	
		
		String declstr = "";
		for (Node dec : declist) {
			declstr += dec.toPrint(s + "  ");
		}
		
		return s + "ProgLetIn\n" + clstr + declstr + exp.toPrint(s + "  ");
	}

	public Node typeCheck() {
		for (Node cl : cllist) {
			cl.typeCheck();
		}
		
		for (Node dec : declist) {
			dec.typeCheck();
		}
		
		return exp.typeCheck();
	}

	public String codeGeneration() {
		String clCode = "";
		for (Node cl : cllist)
			clCode  += cl.codeGeneration();
		
		String declCode = "";
		for (Node dec : declist)
			declCode += dec.codeGeneration();
		
		return "push 0\n" + 
					clCode +
					declCode +
					exp.codeGeneration() +
					"halt\n" + 
					FOOLlib.getCode();
	}

}