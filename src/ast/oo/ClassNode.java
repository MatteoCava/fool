package ast.oo;

import java.util.ArrayList;
import java.util.List;

import ast.ArrowTypeNode;
import ast.DecNode;
import ast.Node;
import ast.STentry;
import lib.FOOLlib;

public class ClassNode implements Node, DecNode {

	private String id;
	private ArrayList<Node> fields = new ArrayList<Node>(); 
	private ArrayList<Node> methods = new ArrayList<Node>(); //sono i figlio VERI, non ereditati
	private Node symType; //tipo ClassTypeNode
	private STentry superEntry; //pallina del padre per quando si eredita

	public ClassNode(String id) {
		this.id = id;
	}

	public void setSymType(Node symType) {
		this.symType = symType;
	}

	@Override
	public String toPrint(String s) {
		String fieldsString = "";
		for (Node field : fields) {
			fieldsString += field.toPrint(s + "  ");
		}
		String methodsString = "";
		for (Node method : methods) {
			methodsString += method.toPrint(s + "  ");
		}
		
		return s + "Class: " + id + "\n" +
			fieldsString +
			methodsString;
	}

	@Override
	public Node typeCheck() {
		
		
		for (Node field : fields) {
			field.typeCheck();
			if (this.superEntry != null) {
				ClassTypeNode superClassTypeNode = (ClassTypeNode) superEntry.getType();
				int index = -((FieldNode)field).getOffset() -1; //i campi partono da -1
				if (superClassTypeNode.getAllFields().size() > index) { //field ovverride
					if (!FOOLlib.isSubtype(((FieldNode)field).getSymType(),
							superClassTypeNode.getAllFields().get(index))) {
						System.out.println("[ClassNode] Wrong type override of  " + index + "  parameter of class " + id);
						System.exit(0);
					}
				}
			}
		}
		
		for (Node method : methods) {
			method.typeCheck();
			if (this.superEntry != null) {
				ClassTypeNode superClassTypeNode = (ClassTypeNode) superEntry.getType();
				int index = ((MethodNode)method).getOffset(); //i metodi partono da 0
				if (superClassTypeNode.getAllMethods().size() > index) { //method ovverride
					if (!FOOLlib.isSubtype(((MethodNode)method).getSymType(), 
							superClassTypeNode.getAllMethods().get(index))) {
						System.out.println("[ClassNode] Wrong type override for method " + index + " of class " + id);
						System.exit(0);
					}	
				}
				
			}
			
		}	
		return null;
	}

	/**
	 * Ritorna codice che alloca sullo heap la dispatch table della clsse 
	 * e lascia il dispatch pointer sullo stack
	 */
	@Override
	public String codeGeneration() {
		if (this.superEntry != null) {
			//creata copiando la dispatch table della classe da cui eredita
			FOOLlib.addDispatchTable(-this.superEntry.getOffset() - 2);
		} else {
			FOOLlib.addDispatchTable();
		}
		
		for (Node m : methods) {
			m.codeGeneration();

			FOOLlib.addMethodToDispatchTable(((MethodNode)m).getOffset(), 
					((MethodNode)m).getLabel());
		}
		
		String classCode = "lhp\n";  //dispatch pointer da ritornare alla fine
		
		for (String label : FOOLlib.getCurrentDispatchTable()) {
			String classMethodCode = "push "+ label + "\n" +
					"lhp\n" + //metto l'etichetta sullo heap: dove sta hp
					"sw\n" + 
					"lhp\n" + //incremento hp
					"push 1\n" + 
					"add\n" +
					"shp\n";
			
			classCode += classMethodCode;
		}
		
		return classCode;
		
	}

	@Override
	public Node getSymType() {
		return this.symType;
	}

	public void addField(Node field) {
		this.fields.add(field);
	}

	public void addMethod(Node method) {
		this.methods.add(method);
	}

	public STentry getSuperEntry() {
		return superEntry;
	}

	public void setSuperEntry(STentry superEntry) {
		this.superEntry = superEntry;
	}
	
	
	public ArrayList<Node> getFields() {
		return fields;
	}

	public ArrayList<Node> getMethods() {
		return methods;
	}

	public void addFields(ArrayList<Node> fields) {
		this.fields.addAll(fields);
	}

	public void addMethods(ArrayList<Node> methods) {
		this.methods.addAll(methods);
	}
	

}
