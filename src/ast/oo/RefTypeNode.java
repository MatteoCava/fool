package ast.oo;

import ast.Node;

public class RefTypeNode implements Node {

	private String classId;

	public RefTypeNode(String classId) {
		this.classId = classId;
	}

	@Override
	public String toPrint(String s) {
		return s + "RefTypeNode: " + this.classId + "\n";
	}

	// non utilizzato
	@Deprecated
	public Node typeCheck() {
		return null;
	}

	// non utilizzato
	@Deprecated
	public String codeGeneration() {
		return "";
	}
	
	public String getClassId() {
		return this.classId;
	}

}
