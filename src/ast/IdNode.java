package ast;

import ast.oo.ClassTypeNode;

public class IdNode implements Node {

	private String id;
	private int nestingLevel;
	private STentry entry;

	public IdNode(String i, STentry st, int nl) {
		id = i;
		nestingLevel = nl;
		entry = st;
	}

	public String toPrint(String s) {
		return s + "Id:" + id + " at nestinglevel " + nestingLevel + "\n" + entry.toPrint(s + "  ");
	}
	
	public Node typeCheck() {
		if (entry.isMethod()) {
			System.out.println("[IdNode] id cannot be a method");
			System.exit(0);
		}
		if (entry.getType() instanceof ClassTypeNode) {
			System.out.println("[IdNode] id cannot be a a class");
			System.exit(0);
		}
		return entry.getType();
	}

	public String codeGeneration() {
		String getAR = "";
		for (int i = 0; i < nestingLevel - entry.getNestinglevel(); i++) 
			getAR += "lw\n";
		
		String returnStr =  //indirizzo all'ar in cui la funzione � dichiarata
				"push " + 
				entry.getOffset() + "\n" + 
				"lfp\n" + 
				getAR + // risalgo la catena statica per ottenere l'indirizzo dell'AR in cui � dichiarata
				"add\n" +
				"lw\n";
		
		if (entry.getType() instanceof ArrowTypeNode) { //aggiunta indirizzo della funzione
			returnStr += ("push " + (entry.getOffset() - 1) + "\n" + 
					"lfp\n" + 
					getAR + 
					"add\n" + 
					"lw\n");
		}
		
		return returnStr; 
	}

}