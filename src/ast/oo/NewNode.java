package ast.oo;

import ast.Node;
import java.util.ArrayList;
import java.util.List;

import ast.STentry;
import lib.FOOLlib;

public class NewNode implements Node {

	private String id;
	private STentry entry;
	private ArrayList<Node> argList;

	public NewNode(String id, STentry entry, ArrayList<Node> argList) {
		this.id = id;
		this.entry = entry;
		this.argList = argList;
	}

	@Override
	public String toPrint(String s) {
		String argsString = "";
		for (Node arg : argList) {
			argsString += arg.toPrint(s + "  ");
		}
		return s + "NewNode: " + id + "\n" + entry.toPrint(s + "  ") + argsString;
	}

	@Override
	public Node typeCheck() {
		List<Node> entryFields = ((ClassTypeNode) entry.getType()).getAllFields();
		
		if (entryFields.size() != argList.size()) {
			System.out.println("[NewNode] Wrong parameter number for " + id + ", expected " + entryFields.size());
			System.exit(0);
		}
		
		for (int i = 0; i < argList.size(); i++) {
			argList.get(i).typeCheck();
			
			if (!FOOLlib.isSubtype(argList.get(i).typeCheck(), entryFields.get(i))) {
				System.out.println("[NewNode] Wrong type for " + (i + 1) + "-th parameter in the invocation of " + id);
				System.exit(0);
			}

		}

		return new RefTypeNode(id);
	}

	@Override
	public String codeGeneration() {
		
		String parCode = "";
		for (int i = 0; i < argList.size(); i++) {
			parCode += argList.get(i).codeGeneration();
		}
		
		String parHeapSw = "";
	
		for (int i = 0; i < argList.size(); i++) {
			String parMethodCode =
					// metto sullo heap ciascun parametro
					"lhp\n" + 
					"sw\n" +
					// aumento di 1 lo heap
					"lhp\n" +
					"push 1\n" + 
					"add\n" +
					"shp\n";
			
			parHeapSw += parMethodCode;
		}
		
		return  parCode + 
				parHeapSw +
				
				//scrive a indirizzo hp il dispatch pointer
				"push " + (FOOLlib.MEMSIZE+entry.getOffset()) + "\n" +  
				"lw\n" +
				"lhp\n" +
				"sw\n" +
				
				//a sto pounto devo ritornare l'object pointer(il valore di hp in questo momento)
				"lhp\n" + //e' il valore che la new deve ritornare
				
				//incremento hp
				"lhp\n" +
				"push 1\n" + 
				"add\n" +
				"shp\n";
	}

}
