package lib;

import java.util.ArrayList;
import java.util.HashMap;
import ast.*;
import ast.oo.EmptyTypeNode;
import ast.oo.RefTypeNode;

public class FOOLlib {
	
	 public static final int MEMSIZE = 10000; 
	
	// indice per generare nuove etichette
	private static int labCount = 0;
	
	// indice per generare nuove etichette per le funzioni
	private static int funLabCount = 0;
	
	/**
	 * Codice delle funzioni. Viene incrementato man mano che si incontrano funzioni e
	 * stampato alla fine della code generation
	 */
	private static String funCode = "";
	
	/**
	 * Dispatch table per l'accesso alle classi e ai loro metodi.
	 * Una per ogni classe.
	 */
	private static ArrayList<ArrayList<String>> dispatchTables = new ArrayList<ArrayList<String>>();
	
	/**
	 * Gerarchia di tipo Type-SuperType per le classi
	 */
	private static HashMap<String, String> superTypes = new HashMap<String, String>();
	
	/**
	 * Valuta se il tipo "a"  <= al tipo "b"
	 * 
	 * @param a
	 * @param b
	 * @return "a" isSubtype of "b"
	 */
	public static boolean isSubtype(Node a, Node b) {
		if (a instanceof ArrowTypeNode && b instanceof ArrowTypeNode) {
			ArrowTypeNode subtype = (ArrowTypeNode) a;
			ArrowTypeNode supertype = (ArrowTypeNode) b;
			ArrayList<Node> subParlist = subtype.getParList();
			ArrayList<Node> superParList = supertype.getParList();
			if (subParlist.size() == superParList.size()) {
				for (int i = 0; i < subParlist.size(); i++) {
					// Controllo sulla controvarianza dei parametri
					if (!isSubtype(superParList.get(i), subParlist.get(i))) {
						System.out.println("FOOLlib " + i + "-th parameter not subtype");
						return false;
					}
				}
				// controllo sulla covarianza del tipo di ritorno
				if (!isSubtype(subtype.getRet(), supertype.getRet())) {
					System.out.println("FOOLlib different return types");
					return false;
				}
				return true;

			} else {
				return false;
			}
		}
		
		if (a instanceof RefTypeNode && b instanceof RefTypeNode) {
			String aClassId = ((RefTypeNode)a).getClassId();
			String bClassId = ((RefTypeNode)b).getClassId();
			if ( aClassId.equals(bClassId)) {
				return true;
			} else {
				return isSubtypeByHierarchy(aClassId, bClassId);
			}
		}
		
		if (a instanceof EmptyTypeNode && b instanceof RefTypeNode) {
			return true;		
		}
		
		return a.getClass().equals(b.getClass()) || ((a instanceof BoolTypeNode) && (b instanceof IntTypeNode));
	}
	
	/**
	 * Recursive method which checks if a classId is a subtype of another.
	 * @param subtype
	 * @param supertype
	 * @return
	 */
	private static boolean isSubtypeByHierarchy(String subtype, String supertype) {
		String supertypeFound = getSuperType(subtype);
		if (supertypeFound == null) {
			return false;
		} else if (supertypeFound.equals(supertype)) {
			return true;
		} else {
			return isSubtypeByHierarchy(supertypeFound, supertype);
		}
	}
		
	/**
	 * Generate a new label
	 * @return the new label
	 */
	public static String freshLabel() {
		return "label" + (labCount++);
	}

	/**
	 * Generate a new function label
	 * @return the new function label
	 */
	public static String freshFunLabel() {
		return "function" + (funLabCount++);
	}

	/**
	 * Aggiunge il nuovo codice a quello delle funzioni
	 * @param code
	 */
	public static void putCode(String code) {
		funCode += "\n" + code; 	// aggiunge una linea vuota di separazione prima di funzione
	}
	
	/**
	 * Recupera il codice del funzioni
	 * @return funCode
	 */
	public static String getCode() {
		return funCode;
	}
	
	/**
	 * Adds a pair Type-Supertype to the supertype map.
	 * @param type the type to be added
	 * @param supertype the supertype to be added
	 */
	public static void addSuperType(String type, String supertype) {
		superTypes.put(type, supertype);
	}
	
	private static String getSuperType(String type) {
		return superTypes.get(type);
	}
		
	/**
	 * Aggiunge una nuova dispatch table vuota.
	 */
	public static void addDispatchTable() {
		dispatchTables.add(new ArrayList<String>());
	}
	
	/**
	 * Aggiunge una nuova dispatch table clonando quella presente all'indice index.
	 * @param index indice della dispatch table da clonare
	 */
	public static void addDispatchTable(int index) {
		ArrayList<String> newDispatchTable = new ArrayList<String>();
		dispatchTables.get(index).forEach(e -> newDispatchTable.add(e));
		dispatchTables.add(newDispatchTable);
	}
	
	/**
	 * Aggiunge un nuovo metodo, con relativi offset e label alla dispatch table corrente.
	 * @param methodOffset
	 * @param methodLabel
	 */
	public static void addMethodToDispatchTable(int methodOffset, String methodLabel) {
		if (getCurrentDispatchTable().size() > methodOffset) {
			getCurrentDispatchTable().set(methodOffset, methodLabel);
			
		} else {
			getCurrentDispatchTable().add(methodLabel);
		}
		
	}
	
	/**
	 * Ritorna la dispatch table corrente
	 * @return
	 */
	public static ArrayList<String> getCurrentDispatchTable() {
		return dispatchTables.get(dispatchTables.size() - 1);
	}

	/**
	 * Crea una deep copy di una classe ereditata all'interno della class table.
	 * @param superEntry
	 * @return
	 */
	public static HashMap<String,STentry> cloneVirtualTable(HashMap<String,STentry> superEntry) {
		HashMap<String,STentry> newHashMap = new HashMap<String,STentry>();
		superEntry.keySet().stream().forEach(methodName -> {
			newHashMap.put(methodName, superEntry.get(methodName));
		});
		return newHashMap;
	}
	
	public static Node lowestCommonAncestor(Node a, Node b) {
		if (a instanceof EmptyTypeNode && !(b instanceof EmptyTypeNode)){
			return b;
			
		} else if (!(a instanceof EmptyTypeNode) && b instanceof EmptyTypeNode){
			return a;
			
		} else if ((a instanceof IntTypeNode || a instanceof BoolTypeNode) 
				&& (b instanceof IntTypeNode || b instanceof BoolTypeNode)) {
					return (a instanceof IntTypeNode || b instanceof IntTypeNode) ? new IntTypeNode() : new BoolTypeNode();
					
		} else if (a instanceof RefTypeNode && b instanceof RefTypeNode) {
			return refTypeNodeLowestCommonAncestor(b, a);
			
		} else if (a instanceof ArrowTypeNode && b instanceof ArrowTypeNode) {
			
			ArrowTypeNode arrowA = (ArrowTypeNode) a;
			ArrowTypeNode arrowB = (ArrowTypeNode) b;
			
			Node returnNode = lowestCommonAncestor(arrowA.getRet(), arrowB.getRet());
			if (returnNode == null) {
				return null;
			}
			ArrayList<Node> parameterNodes = new ArrayList<>();
			
			for (int i = 0; i < arrowA.getParList().size(); i++) {
				Node aParam = arrowA.getParList().get(i);
				Node bParam = arrowB.getParList().get(i);
				
				if (isSubtype(aParam, bParam)) {
					parameterNodes.add(aParam);
				} else if (isSubtype(bParam, aParam)) {
					parameterNodes.add(bParam);
				} else {
					return null;
				}
				
			}
			
			return new ArrowTypeNode(parameterNodes, returnNode);
			
		}
		
		return null;
	}
	
	/**
	 * Metodo ricorsivo utilizzato dal lowestCommonAncestor nel caso siano entrambi RefTypeNode.
	 * @param subtype
	 * @param supertype
	 * @return
	 */
	private static RefTypeNode refTypeNodeLowestCommonAncestor(Node subtype, Node supertype) {
		if (isSubtype(subtype, supertype)) {
			return (RefTypeNode)supertype;
		} else {
			String superTypeFound = getSuperType(((RefTypeNode)supertype).getClassId());
			if (superTypeFound == null) {
				return null;
			}
			return refTypeNodeLowestCommonAncestor(subtype, new RefTypeNode(superTypeFound));
		}
	}
}
