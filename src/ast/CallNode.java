package ast;

import java.util.ArrayList;

import lib.FOOLlib;

public class CallNode implements Node {

	private String id;
	private int nestingLevel;
	private STentry entry;
	private ArrayList<Node> parlist = new ArrayList<Node>();

	public CallNode(String i, STentry st, ArrayList<Node> p, int nl) {
		id = i;
		nestingLevel = nl;
		entry = st;
		parlist = p;
	}

	public String toPrint(String s) {
		String parlstr = "";
		for (Node par : parlist) {
			parlstr += par.toPrint(s + "  ");
		}

		return s + "Call:" + id + " at nestinglevel " + nestingLevel + "\n" + entry.toPrint(s + "  ") + parlstr;
	}

	public Node typeCheck() {
		ArrowTypeNode t = null;
		if (entry.getType() instanceof ArrowTypeNode) {
			t = (ArrowTypeNode) entry.getType();
		} else {
			System.out.println("Invocation of a non-function " + id);
			System.exit(0);
		}
		ArrayList<Node> p = t.getParList();
		if (!(p.size() == parlist.size())) {
			System.out.println("Wrong number of parameters in the invocation of " + id);
			System.exit(0);
		}

		for (int i = 0; i < parlist.size(); i++) {
			if (!(FOOLlib.isSubtype((parlist.get(i)).typeCheck(), p.get(i)))) {
				System.out.println("Wrong type for " + (i + 1) + "-th parameter in the invocation of " + id);
				System.exit(0);
			}
		}
		return t.getRet();
	}

	public String codeGeneration() {
		String parCode = "";
		for (int i = parlist.size() - 1; i >= 0; i--) {
			parCode += parlist.get(i).codeGeneration();
		}
		
		String getAR = "";
		int nestingLevelDifference = nestingLevel - entry.getNestinglevel();
		
		if (entry.isMethod()) {
			/* mi serve per fare un lw in piu' di risalita per passare dall'oggetto alla dispatch table*/
			nestingLevelDifference++;
		}
		
		for (int i = 0; i < nestingLevelDifference; i++) {
			getAR += "lw\n";
		}
		
//		return "lfp\n" + // Control Link
//				parCode + // allocazione valori parametri
//				"push " + (entry.getOffset()) + "\n" +
//				"lfp\n" + 
//				getAR + 		// risalgo la catena statica per ottenere l'indirizzo dell'AR in cui e' dichiarata la funzione (Access Link)
//				//ci vuole AL: access link, l'activation record in cui la funzione che sto chiamando e' dichiarata // risalgo la catena statica per ottenere l'indirizzo dell'ar in cui e' dichiarata la funzione
//				//e' come quello che ho sotto, ma non devo applicare l'offset per andare a prendere il valore
//				"add\n" + 
//			
//				"lw\n" + 		// carica sullo stack fp della funzione
//				"push " + (entry.getOffset()-1) + "\n" +
//				"lfp\n" + 
//				getAR + // risalgo la catena statica per ottenere
//																			// l'indirizzo dell'AR
//																			// in cui e' dichiarata la funzione (Access Link)
//				"add\n" + 
//				"lw\n" + // carica sullo stack l'indirizzo della funzione
//				"js\n"; // effettua il salto
//
//			
//			//risalgo 2 volte, potrei evitarlo e farlo una volta sola memorizzandomi in una variabile 
			
		
		if (entry.isMethod()) { //FIXME: da ricontrollare, mi sembra fatto al contrario e che manca la parte di ho, pag56
			return "lfp\n" + // Contro Link
					parCode + // allocazione valori parametri
					"lfp\n" + 
					getAR + // risalgo la catena statica per ottenere l'indirizzo dell'AR
										// in cui e' dichiarata la funzione (Access Link)
					
					
					"push " + entry.getOffset() + "\n" +
					"lfp\n" + 
					getAR + // risalgo la catena statica per ottenere
																			// l'indirizzo dell'AR
																			// in cui e' dichiarata la funzione (Access Link)
					"add\n" + 
					"lw\n" + // carica sullo stack l'indirizzo della funzione
					"js\n"; // effettua il salto
			
		} else {
			return  "lfp\n" +		// Control Link 
					parCode +		// allocazione valori parametri
					"push " + (entry.getOffset()) + "\n" +
					"lfp\n" + 
					getAR + 		// risalgo la catena statica per ottenere l'indirizzo dell'AR in cui e' dichiarata la funzione (Access Link)
					//ci vuole AL: access link, l'activation record in cui la funzione che sto chiamando e' dichiarata // risalgo la catena statica per ottenere l'indirizzo dell'ar in cui e' dichiarata la funzione
					//e' come quello che ho sotto, ma non devo applicare l'offset per andare a prendere il valore
					"add\n" + 
					
					"lw\n" + 		// carica sullo stack fp della funzione
					"push " + (entry.getOffset()-1) + "\n" +
					 //se la differenza di nesting e' > 0 aggiungere tanti lw quanto e' la differenza
					"lfp\n" +
					getAR + 		// risalgo la catena statica per ottenere l'indirizzo dell'AR in cui e' dichiarata la funzione (Access Link)
					
					"add\n" + 
					"lw\n" + 		// carica sullo stack l'indirizzo della funzione
					"js\n"; 		// effettua il salto
			
			//con le ultime 4 righe recupero l'indirizzo a cui saltare
			//risalgo 2 volte, potrei evitarlo e farlo una volta sola memorizzandomi in una variabile 
			
		}
		
		
		
	
	}
}