package ast.oo;

import ast.Node;

public class EmptyNode implements Node {

	public String toPrint(String s) {
		return s + "Empty\n";
	}

	@Override
	public Node typeCheck() {
		return new EmptyTypeNode();
	}

	@Override
	public String codeGeneration() {
		return "push -1\n";
	}

}
