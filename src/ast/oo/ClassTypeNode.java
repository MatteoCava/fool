package ast.oo;

import java.util.ArrayList;
import java.util.List;

import ast.Node;

public class ClassTypeNode implements Node {

	//elenco dei tipi dei campi e metodi, anche quelli ereditati
	private List<Node> allFields = new ArrayList<Node>();
	private List<Node> allMethods = new ArrayList<Node>();

	@Override
	public String toPrint(String s) {
		String fieldsString = "";
		for (Node field : allFields) {
			fieldsString += field.toPrint(s + " ");
		}

		String methodsString = "";
		for (Node method : allMethods) {
			methodsString += method.toPrint(s + " ");
		}
		
		return  s + "ClassTypeNode\n"+ 
				s + "Fields types:\n" +
				fieldsString +
				s + "Methods types:\n" +
				methodsString;
	}

	// non utilizzato
	@Deprecated 
	public Node typeCheck() {
		return null;
	}

	// non utilizzato
	@Deprecated
	public String codeGeneration() {
		return "";
	}
	
	/**
	 * Aggiunge il tipo di un campo, inclusi quelle ereditati.
	 * Da utilizzare in ordine di apparizione.
	 * @param node
	 */
	public void addFieldType(Node node) {
		allFields.add(node);
	}

	/**
	 * Aggiunge il tipo di un metodo, inclusi quelle ereditati.
	 * Da utilizzare in ordine di apparizione.
	 * @param node
	 */
	public void addMethodType(Node node) {
		allMethods.add(node);
	}

	/**
	 * Ottiene il tipo di tutti i campi in ordine di apparizione.
	 */
	public List<Node> getAllFields() {
		return this.allFields;
	}
	
	/**
	 * Ottiene il tipo di tutti i metodi in ordine di apparizione.
	 */
	public List<Node> getAllMethods() {
		return this.allMethods;
	}
	
	public void addFieldsType(List<Node> nodes) {
		allFields.addAll(nodes);
	}

	public void addMethodsType(List<Node> nodes) {
		allMethods.addAll(nodes);
	}
	
	public void addOrOverrideFieldType(int index, Node node) {
		if (this.allFields.size() > index) {
			this.allFields.set(index, node);
			
		} else {
			this.addFieldType(node);
		}
	}
	
	public void addOrOverrideMethodType(int index, Node node) {
		if (this.allMethods.size() > index) {
			this.allMethods.set(index, node);
			
		} else {
			this.addMethodType(node);
		}
	}

	

}
