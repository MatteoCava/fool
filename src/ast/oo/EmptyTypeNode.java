package ast.oo;

import ast.Node;

/**
 * Nodo vuoto, da considerare sottotipo di un qualsiasi RefTypeNode
 * @author Luca
 *
 */
public class EmptyTypeNode implements Node {

	@Override
	public String toPrint(String s) {
		return s + "EmptyType\n";
	}

	// non utilizzato
	@Deprecated
	public Node typeCheck() {
		return null;
	}

	// non utilizzato
	@Deprecated
	public String codeGeneration() {
		return "";
	}
}
