package ast;

public class IntTypeNode implements Node {

	public IntTypeNode() {
	}

	public String toPrint(String s) {
		return s + "IntType\n";
	}

	// non utilizzato
	@Deprecated
	public Node typeCheck() {
		return null;
	}

	// non utilizzato
	@Deprecated
	public String codeGeneration() {
		return "";
	}

}