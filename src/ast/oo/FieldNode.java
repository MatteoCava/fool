package ast.oo;

import ast.DecNode;
import ast.Node;

public class FieldNode implements Node, DecNode {
	
	private String id;
	private Node type;
	private int offset;

	public FieldNode(String i, Node t, int offset) {
		this.id = i;
		this.type = t;
		this.offset = offset;
	}

	@Override
	public String toPrint(String s) {
		return s + "Field:" + id + "\n" + type.toPrint(s + "  ");
	}
	
	// non utilizzato
	@Deprecated
	public Node typeCheck() {
		return null;
	}

	// non utilizzato
	@Deprecated
	public String codeGeneration() {
		return "";
	}

	@Override
	public Node getSymType() {
		return type;
	}

	public String getId() {
		return id;
	}

	public int getOffset() {
		return offset;
	}
	
}
