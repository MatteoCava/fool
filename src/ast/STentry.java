package ast;

import ast.oo.*;

public class STentry {

	private int nl;
	private Node type;
	//va settato diversamente in base 
	private int offset;
	// serve per distinguere id funzione da id metodi
	private boolean isMethod = false;

	public STentry(int n, int os) {
		nl = n;
		offset = os;
	}

	public STentry(int n, Node t, int os) {
		nl = n;
		type = t;
		offset = os;
	}

	public void addType(Node t) {
		type = t;
	}

	public Node getType() {
		return type;
	}

	public int getOffset() {
		return offset;
	}

	public int getNestinglevel() {
		return nl;
	}
	
	public void setAsMethod() {
		this.isMethod = true;
	}

	/**
	 * Serve per sapere quando fare un lw in pi� per arrivare alla dispatch table
	 * @return isMethod
	 */
	public boolean isMethod() {
		return this.isMethod;
	}

	public String toPrint(String s) {
		return s + "STentry: nestlev " + Integer.toString(nl) + "\n" + s + "STentry: type\n " + type.toPrint(s + "  ")
				+ s + "STentry: offset " + offset + "\n" + s + "STentry: isMethod: " + isMethod() + "\n";
	}

}