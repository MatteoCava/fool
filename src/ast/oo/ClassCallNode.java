package ast.oo;

import ast.ArrowTypeNode;
import ast.Node;
import ast.STentry;
import lib.FOOLlib;

import java.util.ArrayList;

/**
 * chiamata da fuori di un metodo
 *
 */
public class ClassCallNode implements Node {

	private String id;
	private int nestingLevel;
	private STentry entry;
	private STentry methodEntry;
	private ArrayList<Node> parList;

	public ClassCallNode(String id, STentry entry, STentry methodEntry, ArrayList<Node> parList, int nestingLevel) {
		this.id = id;
		this.nestingLevel = nestingLevel;
		this.entry = entry;
		this.methodEntry = methodEntry;
		this.parList = parList;
	}

	@Override
	public String toPrint(String s) {
		String parsString = "";
		for (Node par : parList) {
			parsString += par.toPrint(s + "  ");
		}
		
		return s + "ClassCallNode: " + id + " at nesting level " + nestingLevel + "\n" +
				s + "  VarEntry:\n" +
				entry.toPrint(s + "    ") +
				s +	"  MethodEntry:\n" +
				methodEntry.toPrint(s + "    ") +
				parsString;
	}

	@Override
	public Node typeCheck() {
		ArrowTypeNode t = null;
		if (methodEntry.getType() instanceof ArrowTypeNode) {
			t = (ArrowTypeNode) methodEntry.getType();
		} else {
			System.out.println("Invocation of a non-method " + id);
			System.exit(0);
		}
		ArrayList<Node> p = t.getParList();
		if (!(p.size() == parList.size())) {
			System.out.println("Wrong number of parameters in the invocation of " + id);
			System.exit(0);
		}

		for (int i = 0; i < parList.size(); i++) {
			if (!(FOOLlib.isSubtype((parList.get(i)).typeCheck(), p.get(i)))) {
				System.out.println("Wrong type for " + (i + 1) + "-th parameter in the invocation of " + id);
				System.exit(0);
			}
		}
		
		return ((ArrowTypeNode)methodEntry.getType()).getRet();
	}

	@Override
	public String codeGeneration() {
		String parCode = "";
		for (int i = parList.size() - 1; i >= 0; i--) {
			parCode += parList.get(i).codeGeneration();
		}
		
		
		String getAR = "";
		for (int i = 0; i < nestingLevel - entry.getNestinglevel(); i++) {
			getAR += "lw\n";
		}
		
		return 	"lfp\n"+ //come per callnode, i parametri dovranno puressere usati 
				parCode + 
				"lfp\n" +
				getAR + 
				"push " + entry.getOffset() + "\n" + //recupera per settare l'al
				"add\n" + 
				"lw\n" + //object pointer messo sullo stack
				
				"lfp\n" + 
				getAR + 
				"push " + entry.getOffset() + "\n" + 
				"add\n" + 
				"lw\n" + //carico object pointer
				"lw\n" + //carico dispatch pointer
				"push " + methodEntry.getOffset() + "\n" + //aggiunta offset metodo
				"add\n" + //aggiungo offset per arrivare al metodo
				"lw\n" + //carico indirizzo metodo a cui saltare dalla dispatch table
				"js\n"; //salto al metodo
	}

}
