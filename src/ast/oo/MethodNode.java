package ast.oo;

import java.util.ArrayList;

import ast.ArrowTypeNode;
import ast.DecNode;
import ast.FunNode;
import ast.Node;
import lib.FOOLlib;

public class MethodNode implements Node, DecNode {
	
	private String id;
	private Node type;
	private ArrayList<Node> parlist = new ArrayList<Node>(); // campo "parlist" che � lista di Node
	private ArrayList<Node> declist = new ArrayList<Node>();
	private Node exp;
	private String label;
	private int offset; //durante il parser verra settato all'offset messo in symbol table
	private Node symType;

	public MethodNode(String id, int offset, Node type) {
		this.offset = offset;
		this.id = id;
		this.type = type;
	}
	
	@Override
	public String toPrint(String s) {
		String parlstr = "";
		for (Node par : parlist) {
			parlstr += par.toPrint(s + "  ");
		}

		String declstr = "";
		for (Node dec : declist) {
			declstr += dec.toPrint(s + "  ");
		}

		return s + "Method:" + id + "\n" + 
				type.toPrint(s + "  ") + 
				parlstr + 
				declstr + 
				exp.toPrint(s + "  ");
	
	}
	
	/**
	 * Adds a list of declaration to MethodNode
	 * @param decList
	 */
	public void addDecList(ArrayList<Node> decList) {
		this.declist = decList;
	}

	/**
	 * Adds the body of the MethodNode
	 * @param body
	 */
	public void addBody(Node body) {
		exp = body;
	}
	
	public void addParList(ArrayList<Node> parList) {
		this.parlist = parList;
	}

	/**
	 * Adds a parameter to parameter list of MethodNode
	 * @param par
	 */
	public void addPar(Node par) {
		parlist.add(par);
	}
	
	/**
	 * Used by ClassNode to retrieve the method offset in Code Generation.
	 * @return the method offset.
	 */
	public int getOffset() {
		return this.offset;
	}
	
	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	/**
	 * Used by ClassNode to retrieve the method label in Code Generation.
	 * @return the method label.
	 */
	public String getLabel() {
		return this.label;
	}

	public String getId() {
		return id;
	}

	@Override
	public Node typeCheck() {
		for (Node dec : declist) {
			dec.typeCheck();
		}
		;
		if (!FOOLlib.isSubtype(exp.typeCheck(), type)) {
			System.out.println("Incompatible value for variable");
			System.exit(0);
		}
		return null;
	}

	@Override
	public String codeGeneration() {
		String declCode = "";
		for (Node dec : declist) {
			declCode += dec.codeGeneration();
		}
		
		String popDecl = "";
		for (Node dec : declist) {
			if(((DecNode) dec).getSymType() instanceof ArrowTypeNode) {
				popDecl += "pop\n";
			}
			popDecl += "pop\n";
		}

		String popParl = "";
		for (Node par : parlist) {
			if(((DecNode) par).getSymType() instanceof ArrowTypeNode) {
				popParl += "pop\n";
			}
			popParl += "pop\n";
		}
		
		// genero una nuova etichetta e la metto nel campo "label"
		this.label = FOOLlib.freshFunLabel();

		// Code generation identica a quella di FunNode
		FOOLlib.putCode(this.label + ":\n" +
				"cfp\n" + // setta $fp a $sp
				"lra\n" + // inserisce return address
				declCode + // inserisce dichiarazioni locali
				exp.codeGeneration() + "srv\n" + // pop del return value
				popDecl + // pop delle dichiarazioni
				"sra\n" + // pop del return address
				"pop\n" + // pop di AL
				popParl + // pop dei parametri
				"sfp\n" + // setto $fp al valore del CL
				"lrv\n" + // risultato della funzione sullo stack
				"lra\n" + 
				"js\n" // salta a $ra
		);

		// non ritorno nulla. La label la trovo dentro al metodo.
		return  "";
	}

	public void setSymType(Node symType) {
		this.symType = symType;
	}
	
	@Override
	public Node getSymType() {
		return this.symType;
	}

}
